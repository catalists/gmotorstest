//
//  ErrorResponseModel.swift
//  GMotorTest
//
//  Created by Umaid Saleem on 7/5/21.
//

import Foundation

public struct ErrorResponseModel {
    var code: String?
    var message: String?
    
    init(code: String?, message: String?) {
        self.code = code
        self.message = message
    }
    
    init(data: Data) {
        do {
            // make sure this JSON is in the format we expect
            if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                self.code = json["httpStatusCode"] as? String
                self.message = json["errorMessage"] as? String
            }
        } catch let error as NSError {
            Log.print(.error, "Failed to load: \(error.localizedDescription)")
        }
    }
}
