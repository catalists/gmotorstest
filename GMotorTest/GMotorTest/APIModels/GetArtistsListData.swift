//
//  GetArtistsListData.swift
//  GMotorTest
//
//  Created by Umaid Saleem on 7/5/21.
//

import Foundation

struct GetArtistsListData {
    
    // MARK: - Response Struct
    struct Response: Codable {
        let resultCount: Int?
        let results: [ArtistLists]?
        
        struct ArtistLists: Codable {
            let wrapperType, kind, artistName, collectionName, collectionCensoredName : String?
            let artistViewUrl, collectionViewUrl, artworkUrl30, artworkUrl60, artworkUrl100 : String?
            let artistId, collectionId, trackCount, trackId, collectionArtistId : Int?
            let discCount, discNumber, trackNumber, trackTimeMillis : Int?
            let collectionArtistViewUrl, trackViewUrl, contentAdvisoryRating, shortDescription, longDescription, feedUrl : String?
            let collectionPrice, trackPrice, trackRentalPrice, collectionHdPrice, trackHdPrice, trackHdRentalPrice: Double?
            let collectionExplicitness, country, currency, releaseDate, primaryGenreName, previewUrl: String?
            let description, trackName, trackCensoredName, trackExplicitness, artworkUrl600: String?
            let hasITunesExtras: Bool?
            let genreIds: [String]?
            let genres: [String]?
        }
    }
}
