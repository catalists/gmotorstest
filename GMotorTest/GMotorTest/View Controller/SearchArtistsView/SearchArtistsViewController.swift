//
//  SearchArtistsViewController.swift
//  GMotorTest
//
//  Created by Umaid Saleem on 7/5/21.
//

import UIKit

class SearchArtistsViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var artistTxtFd: UITextField!
    @IBOutlet weak var searchBtn: UIButton!
    
    // MARK: - Properties
    var viewModel = SearchArtistViewModel()
    let activityIndicator = ArtistsLoadingView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        setAccessibilityIdentifiers()
    }
    
    // MARK: - Setup UI
    private func setup() {
        title = StringConstants.View.searchArtistView
        viewModel.delegate = self
        view.backgroundColor = UIColor(named: "white_charcoal")
        // Show the navigation bar with large title and make it small while scrolling
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.sizeToFit()
    }
    
    
    // MARK: - Setup Accessibility
    private func setAccessibilityIdentifiers() {
        view.accessibilityIdentifier = "SearchArtistsViewController"
        artistTxtFd.accessibilityIdentifier = "artistTxtFd"
        searchBtn.accessibilityIdentifier = "searchBtn"
    }
    
    @IBAction func searchArtists() {
        guard artistTxtFd.text?.isEmpty == false else { return }
        viewModel.getArtistLists(artistName: artistTxtFd.text ?? "")
    }
}

// MARK: - SearchArtistViewModel Protocols
extension SearchArtistsViewController: SearchArtistViewModelProtocol {
    
    func loadData(_ results: [GetArtistsListData.Response.ArtistLists]) {
        guard !results.isEmpty else { return }
        
        // Call Search Artist list view controller
        DispatchQueue.main.async {
            guard let searchArtistListViewController = SearchArtistsListViewController.storyboard.instantiateViewController(
                    withIdentifier: "SearchArtistsListViewController") as? SearchArtistsListViewController else { return }
            searchArtistListViewController.itunesMusicListData = results
            self.navigationController?.pushViewController(searchArtistListViewController, animated: true)
        }
    }
    
    func showActivityIndicator() {
        DispatchQueue.main.async {
            self.activityIndicator.show(on: self.view)
        }
    }
    
    func hideActivityIndicator() {
        DispatchQueue.main.async {
            self.activityIndicator.hide()
        }
    }
    
    func handleAPIError(apiName: String) {
        DispatchQueue.main.async {
            self.showToast(message: "\(StringConstants.ErrorMessage.apiError) from \(apiName)")
        }
    }
}

// MARK: - Additional Helper class
extension SearchArtistsViewController {
    
    func showToast(message : String) {
        // This is reusable method from UI Components
        ArtistsToast().displayToast(on: self.view, message: message, font: .systemFont(ofSize: 14.0))
    }
}
