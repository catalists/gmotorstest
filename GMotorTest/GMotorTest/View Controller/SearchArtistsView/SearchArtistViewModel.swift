//
//  SearchArtistViewModel.swift
//  GMotorTest
//
//  Created by Umaid Saleem on 7/5/21.
//

import Foundation

protocol SearchArtistViewModelProtocol: AnyObject {
    func loadData(_ results: [GetArtistsListData.Response.ArtistLists])
    func showActivityIndicator()
    func hideActivityIndicator()
    func handleAPIError(apiName: String)
}

struct SearchArtistViewModel {
    
    weak var delegate: SearchArtistViewModelProtocol?
    
    func getArtistLists(artistName: String) {
        
        delegate?.showActivityIndicator()
        GetArtistsListDataLayer().retrieveData(artistName: artistName, { (response) in
            switch response {
            case .success(let responseModel):
                self.delegate?.hideActivityIndicator()
                delegate?.loadData(responseModel.results ?? [])
            case .failure(let error):
                Log.print(.error, error)
                self.delegate?.hideActivityIndicator()
                delegate?.handleAPIError(apiName: "GetArtistsListData")
            }
        })
    }
}
