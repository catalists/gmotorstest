//
//  SearchArtistsListViewModel.swift
//  GMotorTest
//
//  Created by Umaid Saleem on 7/5/21.
//

import Foundation

protocol SearchArtistsListViewModelProtocol: AnyObject {
    func loadTableData(_ tableDataRows: [TableCellModel])
}

struct SearchArtistsListViewModel {
    
    weak var delegate: SearchArtistsListViewModelProtocol?
    
    func populateTableData(itunesMusicListData: [GetArtistsListData.Response.ArtistLists]) {
        var tableData: [TableCellModel] = []
        
        for itunesMusic in itunesMusicListData {
            // Create cell model object
            var searchArtistData = SearchArtistsObject()
            searchArtistData.artistName = itunesMusic.artistName
            searchArtistData.trackName = itunesMusic.trackName
            searchArtistData.trackPrice = itunesMusic.trackPrice
            searchArtistData.releaseDate = itunesMusic.releaseDate
            searchArtistData.primaryGenreName = itunesMusic.primaryGenreName
            
            let model = SearchArtistsListCellModel(searchArtist: searchArtistData, isSelected: false)
            tableData.append(model)
        }
        
        self.delegate?.loadTableData(tableData)
    }
}
