//
//  SearchArtistsListCell.swift
//  GMotorTest
//
//  Created by Umaid Saleem on 7/5/21.
//

import UIKit

struct SearchArtistsObject {
    var artistName: String?
    var trackName: String?
    var trackPrice: Double?
    var releaseDate: String?
    var primaryGenreName: String?
}

struct SearchArtistsListCellModel: TableCellModel {
    
    // MARK: - Properties
    var searchArtist: SearchArtistsObject?
    var isSelected = false
    
    init(searchArtist: SearchArtistsObject, isSelected: Bool) {
        self.searchArtist = searchArtist
        self.isSelected = isSelected
    }
    
    func cellInstance(_ tableView: UITableView, indexPath: IndexPath, delegate: AnyObject?) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SearchArtistsListCell", for: indexPath)
                as? SearchArtistsListCell else { return UITableViewCell() }
        cell.configure(viewModel: self, indexPath: indexPath)
        return cell
    }
}

class SearchArtistsListCell: UITableViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var artistNameLbl: UILabel!
    @IBOutlet weak var trackNameLbl: UILabel!
    @IBOutlet weak var releaseDateLbl: UILabel!
    @IBOutlet weak var primaryGenreNameLbl: UILabel!
    @IBOutlet weak var trackPriceLbl: UILabel!
    @IBOutlet weak var arrowButton: UIButton!
    
    // MARK: - Properties
    var viewModel: SearchArtistsListCellModel?
    var indexPath = IndexPath(row: 0, section: 0)
    var searchArtist: SearchArtistsObject?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    // MARK: - Setup methods
    private func setup() {
        backgroundColor = UIColor(named: "white_charcoal")
        artistNameLbl.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        trackNameLbl.font = UIFont.systemFont(ofSize: 15)
        primaryGenreNameLbl.font = UIFont.systemFont(ofSize: 15)
        releaseDateLbl.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        trackPriceLbl.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        arrowButton.tintColor = UIColor(named: "nickel")
        arrowButton.setImage(UIImage(named: "rightArrow"), for: .normal)
    }
    
    func configure(viewModel: SearchArtistsListCellModel, indexPath: IndexPath) {
        self.viewModel = viewModel
        self.indexPath = indexPath
        artistNameLbl.text = viewModel.searchArtist?.artistName
        trackNameLbl.text = viewModel.searchArtist?.trackName
        primaryGenreNameLbl.text = viewModel.searchArtist?.primaryGenreName
        releaseDateLbl.text = viewModel.searchArtist?.releaseDate
        trackPriceLbl.text = "\(viewModel.searchArtist?.trackPrice ?? 0)"
        
        setAccessibilityIdentifiers(index: indexPath.row)
        setCellSelected(viewModel.isSelected)
    }
    
    func setAccessibilityIdentifiers(index: Int) {
        artistNameLbl.accessibilityIdentifier =  "artistNameLbl\(index)"
        trackNameLbl.accessibilityIdentifier = "trackNameLbl\(index)"
        primaryGenreNameLbl.accessibilityIdentifier = "primaryGenreNameLbl\(index)"
        releaseDateLbl.accessibilityIdentifier = "releaseDateLbl\(index)"
        trackPriceLbl.accessibilityIdentifier = "trackPriceLbl\(index)"
        arrowButton.accessibilityIdentifier = "arrowButton\(index)"
    }
    
    func setCellSelected(_ selected: Bool) {
        if selected {
            arrowButton.tintColor = UIColor(named: "stratosphere_troposphere")
            backgroundColor = UIColor(named: "cirrus_charcoal")
        } else {
            arrowButton.tintColor = UIColor(named: "nickel")
            backgroundColor = UIColor(named: "white_carbon")
        }
        layoutIfNeeded()
    }
    
}
