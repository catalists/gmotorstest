//
//  SearchArtistsListViewController.swift
//  GMotorTest
//
//  Created by Umaid Saleem on 7/5/21.
//

import UIKit

class SearchArtistsListViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var artistTableView: UITableView!
    
    // MARK: - Properties
    static var storyboard = UIStoryboard(name: "Main", bundle: nil)
    var itunesMusicListData = [GetArtistsListData.Response.ArtistLists]()
    var tableData = [TableCellModel]()
    var viewModel = SearchArtistsListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        setupViewModel()
        setAccessibilityIdentifiers()
    }
    
    // MARK: - Setup UI
    private func setup() {
        title = StringConstants.View.searchArtistViewList
        view.backgroundColor = UIColor(named: "white_charcoal")
        // Show the navigation bar with large title and make it small while scrolling
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.sizeToFit()
        setupTableView()
    }
    
    private func setupViewModel() {
        viewModel.delegate = self
        if !itunesMusicListData.isEmpty {
            viewModel.populateTableData(itunesMusicListData: itunesMusicListData)
        }
    }
    
    // MARK: - Setup Accessibility
    private func setAccessibilityIdentifiers() {
        view.accessibilityIdentifier = "SearchArtistsViewController"
        artistTableView.accessibilityIdentifier = "artistTableView"
    }
    
    private func reloadVisibleCells() {
        guard let visibleCells = artistTableView.indexPathsForVisibleRows else { return }
        artistTableView.reloadRows(at: visibleCells, with: .automatic)
    }
}

// MARK: - TableView Delegate Functions
extension SearchArtistsListViewController: UITableViewDelegate, UITableViewDataSource {
    private func setupTableView() {
        artistTableView.delegate = self
        artistTableView.dataSource = self
        artistTableView.backgroundColor = UIColor.clear
        artistTableView.separatorStyle = .none
        artistTableView.estimatedRowHeight = 120
        artistTableView.rowHeight = UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableData[indexPath.row].cellInstance(tableView, indexPath: indexPath, delegate: self)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableRowSelected(indexPath: indexPath)
    }
    
    private func tableRowSelected(indexPath: IndexPath) {
        guard var model = tableData[indexPath.row] as? SearchArtistsListCellModel,
              let cell = artistTableView.cellForRow(at: indexPath) as? SearchArtistsListCell else { return }
        
        // Set model selected and reload visible cells
        model.isSelected = !model.isSelected
        tableData.remove(at: indexPath.row)
        tableData.insert(model, at: indexPath.row)
        cell.setCellSelected(true)
        reloadVisibleCells()
    }
}

// MARK: - SearchArtistsListViewModel Protocols
extension SearchArtistsListViewController: SearchArtistsListViewModelProtocol {
    func loadTableData(_ tableDataRows: [TableCellModel]) {
        tableData = tableDataRows
    }
}
