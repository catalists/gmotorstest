//
//  Reachability.swift
//  GMotorTest
//
//  Created by Umaid Saleem on 7/5/21.
//

import Foundation
import SystemConfiguration
import CoreTelephony

enum ReachabilityType {
    case cellular
    case wifi
    case notConnected
}

class Reachability {
    
    // MARK: - Public Functions
    
    /// Check if connected to the network
    /// - returns: Online (true) or Offline (false)
    func networkConnected() -> Bool {
        guard let defaultRouteReachability = getDefaultReachability() else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) == false {
            return false
        }
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        
        return isReachable && !needsConnection
    }
    
    /// Check network connection type
    /// - returns: `ReachabilityType` - cellular, wifi or notConnected
    func networkType() -> ReachabilityType {
        
        guard let defaultRouteReachability = getDefaultReachability() else {
            return .notConnected
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) == false {
            return .notConnected
        }
        
        let isWifi = flags == .reachable && !(flags == .connectionRequired)
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let isCellular = isReachable && !needsConnection
        
        if isWifi {
            return .wifi
        } else if isCellular {
            return .cellular
        }
        
        return .notConnected
    }
    
    /// Get network connection info as a string
    /// - returns: A string for 'WiFi', Cellular (multiple types) or 'Not Connected'
    func getConnectionInfo() -> String {
        switch networkType() {
        case .wifi:
            return "WiFi"
        case .cellular:
            return getCellularConnectionType()
        case .notConnected:
            return "Not Connected"
        }
    }
    
    // MARK: - Private Functions
    /// Get the default reachability
    /// - returns: An `SCNetworkReachability` object that is the handle to a network address or name
    private func getDefaultReachability() -> SCNetworkReachability? {
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0,
                                      sin_addr: in_addr(s_addr: 0),
                                      sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        return defaultRouteReachability
    }
    
    /// Get the cellular connection type
    /// - returns: A string describing the cellular connection type (2G, 3G, Edge, LTE, etc.)
    private func getCellularConnectionType() -> String {
        let networkInfo = CTTelephonyNetworkInfo()
        let currCarrierType: String?
        if #available(iOS 12.0, *) {
            guard let dict = networkInfo.serviceCurrentRadioAccessTechnology else {
                return "unknown"
            }
            
            let key = dict.keys.first ?? ""
            let carrierType = dict[key]
            
            guard networkInfo.serviceSubscriberCellularProviders != nil &&
                    networkInfo.serviceSubscriberCellularProviders?.isEmpty == false else {
                return "unknown"
            }
            currCarrierType = carrierType
            
        } else {
            // Fall back to pre iOS12
            guard let carrierType = networkInfo.currentRadioAccessTechnology else {
                return "unknown"
            }
            currCarrierType = carrierType
        }
        
        return getCellularDetailFrom(carrierType: currCarrierType)
    }
    
    private func getCellularDetailFrom(carrierType: String?) -> String {
        switch carrierType {
        case CTRadioAccessTechnologyGPRS:
            return "2G" + " (GPRS)"
        case CTRadioAccessTechnologyEdge:
            return "2G" + " (Edge)"
        case CTRadioAccessTechnologyCDMA1x:
            return "2G" + " (CDMA1x)"
        case CTRadioAccessTechnologyWCDMA:
            return "3G" + " (WCDMA)"
        case CTRadioAccessTechnologyHSDPA:
            return "3G" + " (HSDPA)"
        case CTRadioAccessTechnologyHSUPA:
            return "3G" + " (HSUPA)"
        case CTRadioAccessTechnologyCDMAEVDORev0:
            return "3G" + " (CDMAEVDORev0)"
        case CTRadioAccessTechnologyCDMAEVDORevA:
            return "3G" + " (CDMAEVDORevA)"
        case CTRadioAccessTechnologyCDMAEVDORevB:
            return "3G" + " (CDMAEVDORevB)"
        case CTRadioAccessTechnologyeHRPD:
            return "3G" + " (eHRPD)"
        case CTRadioAccessTechnologyLTE:
            return "4G" + " (LTE)"
        default:
            return "unknown"
        }
    }
    
}
