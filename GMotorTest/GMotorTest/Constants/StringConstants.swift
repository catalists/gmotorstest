//
//  StringConstants.swift
//  GMotorTest
//
//  Created by Umaid Saleem on 7/5/21.
//

import Foundation

public enum StringConstants {
    
    // MARK: - View Controller Constants
    public enum View {
        static let searchArtistView = "Search Artists"
        static let searchArtistViewList = "Search Artists List"
    }
    
    // MARK: - Error Constants
    public enum ErrorMessage {
        static let offline = "You appear to be offline. Please connect to the internet and try again."
        static let invalidURL = "Invalid URL"
        static let noData = "No Data Received"
        static let jsonDecodingError = "JSON Decoding Error"
        static let apiError = "Unable to load data from service"
        static let serviceCallFailure = "Request Failed"
        static let errorMessage = "errorMsg"
    }
    
    // MARK: - URL Constants
    public enum URLRequest {
        static let artistListData = "https://itunes.apple.com/search?term="
    }
    
    public enum StaticRequestParams {
        static let timeout = 30
    }
}
