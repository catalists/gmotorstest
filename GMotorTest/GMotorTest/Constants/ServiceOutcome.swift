//
//  ServiceOutcome.swift
//  GMotorTest
//
//  Created by Umaid Saleem on 7/5/21.
//

import Foundation

public enum ServiceOutcome<T> {
    case success(T)
    case failure(ErrorResponseModel)
}
