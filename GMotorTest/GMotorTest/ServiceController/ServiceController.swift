//
//  ServiceController.swift
//  GMotorTest
//
//  Created by Umaid Saleem on 7/5/21.
//

import Foundation

class ServiceController {
    
    var testingParams: [String] = []
    
    func encodeRequestBody<T: Encodable>(requestObject: T, name: String) -> Data? {
        do {
            let jsonEncoder = JSONEncoder()
            let jsonData = try jsonEncoder.encode(requestObject)
            let jsonString = String(data: jsonData, encoding: .utf8)
            Log.print(.verbose, "\(name) Service Request \(jsonString ?? "")")
            return jsonData
        } catch {
            Log.print(.error, "error encoding request")
            return nil
        }
    }
    
}
