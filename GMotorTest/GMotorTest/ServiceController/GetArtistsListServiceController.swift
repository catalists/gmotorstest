//
//  GetArtistsListServiceController.swift
//  GMotorTest
//
//  Created by Umaid Saleem on 7/5/21.
//

import Foundation

protocol GetArtistsListServiceControllerProtocol {
    func getArtistListData(artistName: String,
                           _ completion: @escaping (_ response: ServiceOutcome<GetArtistsListData.Response>) -> Void)
}

class GetArtistsListServiceController: ServiceController, GetArtistsListServiceControllerProtocol {
    
    func getArtistListData(artistName: String,
                           _ completion: @escaping (_ response: ServiceOutcome<GetArtistsListData.Response>) -> Void) {
        
        let encodedText = artistName.addingPercentEncoding(
            withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let urlString = "\(StringConstants.URLRequest.artistListData)\(encodedText)"
        guard let url = URL(string: "\(urlString)") else {
            let errorObj = ErrorResponseModel(code: nil, message: StringConstants.ErrorMessage.invalidURL)
            completion(.failure(errorObj))
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, response, error) in
            // fail completion for Error
            guard let objData = data else {
                let errorObj = ErrorResponseModel(code: nil, message: StringConstants.ErrorMessage.noData)
                completion(.failure(errorObj))
                return
            }
            
            // Validate for blank data and URL response status code
            if let objURLResponse = response as? HTTPURLResponse {
                // We have data validate for JSON and convert in JSON
                do {
                    let getArtistsResponse = try JSONDecoder().decode(GetArtistsListData.Response.self, from: objData)
                    if objURLResponse.statusCode == 200 {
                        completion(.success(getArtistsResponse))
                    }
                } catch {
                    let errorObj = ErrorResponseModel(code: nil, message: StringConstants.ErrorMessage.jsonDecodingError)
                    completion(.failure(errorObj))
                }
            }
        }
        task.resume()
    }
}
