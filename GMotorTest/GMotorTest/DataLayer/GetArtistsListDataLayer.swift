//
//  GetArtistsListDataLayer.swift
//  GMotorTest
//
//  Created by Umaid Saleem on 7/5/21.
//

import Foundation

protocol GetArtistsListDataLayerProtocol {
    func retrieveData(artistName: String,
                      _ completion: @escaping (_ response: ServiceOutcome<GetArtistsListData.Response>) -> Void)
}

class GetArtistsListDataLayer: DataLayer, GetArtistsListDataLayerProtocol {
    
    var callback: ((ServiceOutcome<GetArtistsListData.Response>) -> Void)?
    
    func retrieveData(artistName: String,
                      _ completion: @escaping (_ response: ServiceOutcome<GetArtistsListData.Response>) -> Void) {
        callback = completion
        // if no internet connection, go diretly to the local storage
        guard Reachability().networkConnected() else {
            let err = ErrorResponseModel(code: nil, message: StringConstants.ErrorMessage.offline)
            callback!(.failure(err))
            // We can load offline data from database in case of failures
            return
        }
        
        // call service controller
        GetArtistsListServiceController().getArtistListData(artistName: artistName) { (response) in
            switch response {
            case .success(let model):
                // Callback with updated object
                let responseModel: GetArtistsListData.Response = model
                self.callback!(.success(responseModel))
            case .failure(let error):
                // We can load offline data from database in case of failures
                Log.print(.error, error)
            }
        }
    }
}
