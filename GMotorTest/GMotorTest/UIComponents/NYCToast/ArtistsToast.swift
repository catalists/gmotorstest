//
//  ArtistsToast.swift
//  GMotorTest
//
//  Created by Umaid Saleem on 7/5/21.
//

import UIKit

class ArtistsToast: UIView {
    
    func displayToast(on myView: UIView, message : String, font: UIFont) {
        let toastLabel = UILabel(frame: CGRect(x: myView.frame.size.width/2 - 75,
                                               y: myView.frame.size.height-100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        myView.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}
