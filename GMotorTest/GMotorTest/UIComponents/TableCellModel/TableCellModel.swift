//
//  TableCellModel.swift
//  GMotorTest
//
//  Created by Umaid Saleem on 7/5/21.
//

import UIKit

protocol TableCellModel {
    func cellInstance(_ tableView: UITableView, indexPath: IndexPath, delegate: AnyObject?) -> UITableViewCell
}
