//
//  HTTPResponse.swift
//  GMotorTest
//
//  Created by Umaid Saleem on 7/5/21.
//

import Foundation

public struct HTTPResponse {
    var data: Data?
    var httpURLResponse: HTTPURLResponse?
    var responseDate: Date?
}
