//
//  NetworkManagerProtocol.swift
//  GMotorTest
//
//  Created by Umaid Saleem on 7/5/21.
//

import Foundation

protocol NetworkManagerProtocol {
    func request(_ completion: @escaping (_ response: ServiceOutcome<HTTPResponse>) -> Void)
    var request: URLRequest! { get set }
    var pwdReset: Bool { get set }
}
