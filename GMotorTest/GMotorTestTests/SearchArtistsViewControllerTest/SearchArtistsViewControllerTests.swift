//
//  SearchArtistsViewControllerTests.swift
//  GMotorTestTests
//
//  Created by Umaid Saleem on 7/6/21.
//

import XCTest
@testable import GMotorTest

class SearchArtistsViewControllerTests: XCTestCase {

    private var sut: SearchArtistsViewController!
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    private func initializeViewController() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        sut = storyboard.instantiateViewController(withIdentifier: "SearchArtistsViewController") as? SearchArtistsViewController
        sut.loadViewIfNeeded()
    }
    
    func testOutletsAreConnected() {
        initializeViewController()
        
        // test view controller outlets are connected
        XCTAssertNotNil(sut.artistTxtFd)
        XCTAssertNotNil(sut.searchBtn)
    }
    
    func testSetup() {
        initializeViewController()
        
        XCTAssertEqual(sut.title, StringConstants.View.searchArtistView)
        XCTAssertEqual(sut.view.backgroundColor, UIColor(named: "white_charcoal"))
    }
    
    func testAccessibilityIdentifiers() {
        initializeViewController()
        
        XCTAssertEqual(sut.view.accessibilityIdentifier, "SearchArtistsViewController")
        XCTAssertEqual(sut.artistTxtFd.accessibilityIdentifier, "artistTxtFd")
        XCTAssertEqual(sut.searchBtn.accessibilityIdentifier, "searchBtn")
    }
}
