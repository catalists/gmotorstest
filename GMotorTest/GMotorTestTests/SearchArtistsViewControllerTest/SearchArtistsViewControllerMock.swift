//
//  SearchArtistsViewControllerMock.swift
//  GMotorTestTests
//
//  Created by Umaid Saleem on 7/7/21.
//

import XCTest
@testable import GMotorTest

class SearchArtistsViewControllerMock: UIViewController, SearchArtistViewModelProtocol {
    
    // MARK: - Properties
    var viewModel = SearchArtistViewModel()
    var results = [GetArtistsListData.Response.ArtistLists]()
    var showActivityIndicatorCalled = 0
    var hideActivityIndicatorCalled = 0
    var apiErrorCalled = 0
    
    // MARK: - Expectations
    var handleAPIErrorExpectation: XCTestExpectation?
    var loadDataExpectation: XCTestExpectation?
    var showIndicatorExpectation: XCTestExpectation?
    var hideIndicatorExpectation: XCTestExpectation?
    
    // MARK: - SearchArtistViewModel Methods
    func loadData(_ results: [GetArtistsListData.Response.ArtistLists]) {
        self.results = results
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            self.loadDataExpectation?.fulfill()
        })
    }
    
    func showActivityIndicator() {
        showActivityIndicatorCalled += 1
        showIndicatorExpectation?.fulfill()
    }
    
    func hideActivityIndicator() {
        hideActivityIndicatorCalled += 1
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            self.hideIndicatorExpectation?.fulfill()
        })
    }
    
    func handleAPIError(apiName: String) {
        apiErrorCalled += 1
        handleAPIErrorExpectation?.fulfill()
    }
}


