//
//  SearchArtistViewModelTests.swift
//  GMotorTestTests
//
//  Created by Umaid Saleem on 7/6/21.
//

import XCTest
@testable import GMotorTest

class SearchArtistViewModelTests: XCTestCase {

    private var sut: SearchArtistViewModel!
    private var mockView = SearchArtistsViewControllerMock()
    private var mockServiceController = GetArtistsListServiceControllerMock()
    
    override func setUp() {
        super.setUp()
        sut = SearchArtistViewModel()
        sut.delegate = mockView
        mockView.viewModel = sut
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testEmptyArtistListData() {
        let loadDataExpectation = XCTestExpectation(description: "loadDataExpectation")
        mockView.loadDataExpectation = loadDataExpectation
        let showActivityIndicatorExpectation = XCTestExpectation(description: "showActivityIndicatorExpectation")
        mockView.showIndicatorExpectation = showActivityIndicatorExpectation
        let hideActivityIndicatorExpectation = XCTestExpectation(description: "hideActivityIndicatorExpectation")
        mockView.hideIndicatorExpectation = hideActivityIndicatorExpectation
        
        mockServiceController.testingParams = ["testSuccessEmpty"]
        mockView.viewModel.getArtistLists(artistName: "asddadssdaa")
        wait(for: [loadDataExpectation, showActivityIndicatorExpectation, hideActivityIndicatorExpectation], timeout: 3.0)
        // Validate results
        XCTAssertEqual(mockView.results.count, 0)
        XCTAssertEqual(mockView.showActivityIndicatorCalled, 1)
        XCTAssertEqual(mockView.hideActivityIndicatorCalled, 1)
    }
    
    func testArtistListData() {
        let loadDataExpectation = XCTestExpectation(description: "loadDataExpectation")
        mockView.loadDataExpectation = loadDataExpectation
        let showActivityIndicatorExpectation = XCTestExpectation(description: "showActivityIndicatorExpectation")
        mockView.showIndicatorExpectation = showActivityIndicatorExpectation
        let hideActivityIndicatorExpectation = XCTestExpectation(description: "hideActivityIndicatorExpectation")
        mockView.hideIndicatorExpectation = hideActivityIndicatorExpectation
        
        mockServiceController.testingParams = ["testSuccess"]
        mockView.viewModel.getArtistLists(artistName: "Alex")

        wait(for: [loadDataExpectation, showActivityIndicatorExpectation, hideActivityIndicatorExpectation], timeout: 3.0)
        // Validate results
        XCTAssertEqual(mockView.results.count, 50)
        XCTAssertEqual(mockView.showActivityIndicatorCalled, 1)
        XCTAssertEqual(mockView.hideActivityIndicatorCalled, 1)
        
    }
}
