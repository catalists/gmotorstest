//
//  SearchArtistsListViewControllerMock.swift
//  GMotorTestTests
//
//  Created by Umaid Saleem on 7/7/21.
//

import XCTest
@testable import GMotorTest

class SearchArtistsListViewControllerMock: UIViewController, SearchArtistsListViewModelProtocol {
    
    // MARK: - Properties
    var viewModel = SearchArtistsListViewModel()
    var tableData = [TableCellModel]()
    
    // MARK: - Expectations
    var loadTableDataExpectation: XCTestExpectation?
    
    // MARK: - SearchArtistViewModel Methods
    func loadTableData(_ tableDataRows: [TableCellModel]) {
        tableData = tableDataRows
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            self.loadTableDataExpectation?.fulfill()
        })
    }
}
