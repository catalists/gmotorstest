//
//  SearchArtistsListViewControllerTests.swift
//  GMotorTestTests
//
//  Created by Umaid Saleem on 7/7/21.
//

import XCTest
@testable import GMotorTest

class SearchArtistsListViewControllerTests: XCTestCase {

    private var sut: SearchArtistsListViewController!
    private var searchArtistData = SearchArtistsObject()
    
    private func initializeViewController() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        sut = storyboard.instantiateViewController(withIdentifier: "SearchArtistsListViewController") as? SearchArtistsListViewController
        sut.loadViewIfNeeded()
    }
    
    override func setUp() {
        super.setUp()
        searchArtistData.artistName = "Alex"
        searchArtistData.trackName = "Call me Daddy"
        searchArtistData.trackPrice = 50.0
        searchArtistData.releaseDate = "07/07/2021"
        searchArtistData.primaryGenreName = "Alex Lambart"
        
        initializeViewController()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testOutletsAreConnected() {
        // test view controller outlets are connected
        XCTAssertNotNil(sut.artistTableView)
    }
    
    func testTableViewCellOutletsAreConnected() {
        let model = SearchArtistsListCellModel(searchArtist: searchArtistData, isSelected: false)
        let cell = model.cellInstance(sut.artistTableView, indexPath: IndexPath(row: 0, section: 0), delegate: nil) as! SearchArtistsListCell
        
        // let cell = sut.tableView(sut.artistTableView, cellForRowAt: IndexPath(row: 0, section: 0)) as? SearchArtistsListCell
        XCTAssertNotNil(cell.artistNameLbl)
        XCTAssertNotNil(cell.trackNameLbl)
        XCTAssertNotNil(cell.releaseDateLbl)
        XCTAssertNotNil(cell.primaryGenreNameLbl)
        XCTAssertNotNil(cell.trackPriceLbl)
        XCTAssertNotNil(cell.arrowButton)
    }
    
    func testTableViewHasDelegate() {
        XCTAssertNotNil(sut.artistTableView.delegate)
    }
    
    func testTableViewConfromsToTableViewDelegateProtocol() {
        XCTAssertTrue(sut.conforms(to: UITableViewDelegate.self))
    }
    
    func testTableViewHasDataSource() {
        XCTAssertNotNil(sut.artistTableView.dataSource)
    }
    
    func testTableViewConformsToTableViewDataSourceProtocol() {
        XCTAssertTrue(sut.conforms(to: UITableViewDataSource.self))
        XCTAssertTrue(sut.responds(to: #selector(sut.tableView(_:numberOfRowsInSection:))))
        XCTAssertTrue(sut.responds(to: #selector(sut.tableView(_:cellForRowAt:))))
    }
    
    func testTableViewCellHasReuseIdentifier() {
        let model = SearchArtistsListCellModel(searchArtist: searchArtistData, isSelected: false)
        let cell = model.cellInstance(sut.artistTableView, indexPath: IndexPath(row: 0, section: 0), delegate: nil) as! SearchArtistsListCell
    
        let actualReuseIdentifer = cell.reuseIdentifier
        let expectedReuseIdentifier = "SearchArtistsListCell"
        XCTAssertEqual(actualReuseIdentifer, expectedReuseIdentifier)
    }
    
    func testTableCellHasCorrectLabelText() {
        let model = SearchArtistsListCellModel(searchArtist: searchArtistData, isSelected: false)
        let cell0 = model.cellInstance(sut.artistTableView, indexPath: IndexPath(row: 0, section: 0), delegate: nil) as! SearchArtistsListCell
        
        XCTAssertEqual(cell0.artistNameLbl.text, "Alex")
        XCTAssertEqual(cell0.trackNameLbl.text, "Call me Daddy")
        XCTAssertEqual(cell0.releaseDateLbl.text, "07/07/2021")
        XCTAssertEqual(cell0.primaryGenreNameLbl.text, "Alex Lambart")
        XCTAssertEqual(cell0.trackPriceLbl.text, "\(50.0)")
        XCTAssertEqual(cell0.arrowButton.imageView?.image, UIImage(named: "rightArrow"))
    }
}
