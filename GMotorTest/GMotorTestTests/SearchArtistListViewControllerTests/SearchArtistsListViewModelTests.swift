//
//  SearchArtistsListViewModelTests.swift
//  GMotorTestTests
//
//  Created by Umaid Saleem on 7/7/21.
//

import XCTest
@testable import GMotorTest

class SearchArtistsListViewModelTests: XCTestCase {
    
    private var sut: SearchArtistsListViewModel!
    private var mockView = SearchArtistsListViewControllerMock()
    private var itunesMusicListData = [GetArtistsListData.Response.ArtistLists]()
    
    override func setUp() {
        super.setUp()
        sut = SearchArtistsListViewModel()
        sut.delegate = mockView
        mockView.viewModel = sut
        
        let artistListData1 = GetArtistsListData.Response.ArtistLists(wrapperType: "track", kind: "podcast", artistName: "Alex Cooper", collectionName: "Call Her Daddy", collectionCensoredName: "Call Her Daddy", artistViewUrl: "https://podcasts.apple.com/us/artist/barstool-sports/1524874689?uo=4", collectionViewUrl: "www.google.com", artworkUrl30: "https://is2-ssl.mzstatic.com/image/thumb/Podcasts125/v4/50/2e/3c/502e3c33-8474-534f-298f-835fb945a561/mza_15164629866615628436.jpg/30x30bb.jpg", artworkUrl60: "https://is2-ssl.mzstatic.com/image/thumb/Podcasts125/v4/50/2e/3c/502e3c33-8474-534f-298f-835fb945a561/mza_15164629866615628436.jpg/60x60bb.jpg", artworkUrl100: "https://is2-ssl.mzstatic.com/image/thumb/Podcasts125/v4/50/2e/3c/502e3c33-8474-534f-298f-835fb945a561/mza_15164629866615628436.jpg/100x100bb.jpg", artistId: 1524874689, collectionId: 1418960261, trackCount: 134, trackId: 1418960261, collectionArtistId: 1529, discCount: 45, discNumber: 87, trackNumber: 90, trackTimeMillis: 50, collectionArtistViewUrl: "https://podcasts.apple.com/us/podcast/call-her-daddy/id1418960261?uo=4", trackViewUrl: "https://podcasts.apple.com/us/podcast/call-her-daddy/id1418960261?uo=4", contentAdvisoryRating: "5", shortDescription: "Call Her Daddy", longDescription: "Call Her Daddy", feedUrl: "https://feeds.megaphone.fm/GLT2733274547", collectionPrice: 0, trackPrice: 0, trackRentalPrice: 0, collectionHdPrice: 0, trackHdPrice: 0, trackHdRentalPrice: 0, collectionExplicitness: "explicit", country: "USA", currency: "USD", releaseDate: "2021-05-12T05:10:00Z", primaryGenreName: "Comedy", previewUrl: "", description: "Alex Cooper Call her Daddy", trackName: "Call Her Daddy", trackCensoredName: "Call Her Daddy", trackExplicitness: "explicit", artworkUrl600: "https://is2-ssl.mzstatic.com/image/thumb/Podcasts125/v4/50/2e/3c/502e3c33-8474-534f-298f-835fb945a561/mza_15164629866615628436.jpg/100x100bb.jpg", hasITunesExtras: false, genreIds: ["1303","26"], genres:["Comedy", "Podcasts"])
        itunesMusicListData.append(artistListData1)
        
        let artistListData2 = GetArtistsListData.Response.ArtistLists(wrapperType: "track", kind: "podcast", artistName: "Miley Cirrus", collectionName: "The Last Song", collectionCensoredName: "The Last Song", artistViewUrl: "https://podcasts.apple.com/us/artist/barstool-sports/1524874689?uo=4", collectionViewUrl: "www.google.com", artworkUrl30: "https://www.youtube.com/watch?v=8wxOVn99FTE&ab_channel=MileyCyrusVEVO", artworkUrl60: "https://www.youtube.com/watch?v=8wxOVn99FTE&ab_channel=MileyCyrusVEVO", artworkUrl100: "https://www.youtube.com/watch?v=8wxOVn99FTE&ab_channel=MileyCyrusVEVO", artistId: 1524874689, collectionId: 1418960261, trackCount: 134, trackId: 1418960261, collectionArtistId: 1529, discCount: 45, discNumber: 87, trackNumber: 90, trackTimeMillis: 50, collectionArtistViewUrl: "https://www.youtube.com/watch?v=8wxOVn99FTE&ab_channel=MileyCyrusVEVO", trackViewUrl: "https://www.youtube.com/watch?v=8wxOVn99FTE&ab_channel=MileyCyrusVEVO", contentAdvisoryRating: "5", shortDescription: "When I Look At You", longDescription: "When I Look At You", feedUrl: "https://feeds.megaphone.fm/GLT2733274547", collectionPrice: 0, trackPrice: 0, trackRentalPrice: 0, collectionHdPrice: 0, trackHdPrice: 0, trackHdRentalPrice: 0, collectionExplicitness: "explicit", country: "USA", currency: "USD", releaseDate: "2021-05-12T05:10:00Z", primaryGenreName: "Comedy", previewUrl: "", description: "Miley Cirrus When I Look At You", trackName: "When I Look At You", trackCensoredName: "When I Look At You", trackExplicitness: "explicit", artworkUrl600: "https://is2-ssl.mzstatic.com/image/thumb/Podcasts125/v4/50/2e/3c/502e3c33-8474-534f-298f-835fb945a561/mza_15164629866615628436.jpg/100x100bb.jpg", hasITunesExtras: false, genreIds: ["1303","26"], genres:["Comedy", "Podcasts"])
        
        itunesMusicListData.append(artistListData2)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testEmptyArtistListTableData() {
        let loadTableDataExpectation = XCTestExpectation(description: "loadTableDataExpectation")
        mockView.loadTableDataExpectation = loadTableDataExpectation
        
        mockView.viewModel.populateTableData(itunesMusicListData: [])
        wait(for: [loadTableDataExpectation], timeout: 3.0)
        // Validate results
        XCTAssertEqual(mockView.tableData.count, 0)
    }
    
    func testArtistListTableData() {
        let loadTableDataExpectation = XCTestExpectation(description: "loadDataExpectation")
        mockView.loadTableDataExpectation = loadTableDataExpectation
        
        mockView.viewModel.populateTableData(itunesMusicListData: itunesMusicListData)
        
        wait(for: [loadTableDataExpectation], timeout: 3.0)
        // Validate results
        XCTAssertEqual(mockView.tableData.count, 2)
    }
    
}
