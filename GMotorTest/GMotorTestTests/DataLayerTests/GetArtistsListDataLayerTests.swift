//
//  GetArtistsListDataLayerTests.swift
//  GMotorTestTests
//
//  Created by Umaid Saleem on 7/6/21.
//

import XCTest
@testable import GMotorTest

class GetArtistsListDataLayerTests: XCTestCase {

    private var sut: GetArtistsListDataLayer?
    private var mockServiceController = GetArtistsListServiceControllerMock()

    override func setUp() {
        super.setUp()
        sut = GetArtistsListDataLayer()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testSuccessfulResponse() {
        mockServiceController.testingParams = ["testSuccess"]
        sut?.retrieveData(artistName: "Alex", {(response) in
            switch response {
            case .success(_): break
                // success block is a passing test
            case .failure(_):
                XCTFail()
            }
        })
    }

    func testFailureResponse() {
        mockServiceController.testingParams = ["testFailure"]
        sut?.retrieveData(artistName: "Alex", {(response) in
            switch response {
            case .success(_):
                XCTFail()
            case .failure(let error):
                XCTAssertEqual(error.code, nil)
                XCTAssertEqual(error.message, StringConstants.ErrorMessage.errorMessage)
            }
        })
    }
}

