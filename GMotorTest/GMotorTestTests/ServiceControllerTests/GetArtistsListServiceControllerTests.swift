//
//  GetArtistsListServiceControllerTests.swift
//  GMotorTestTests
//
//  Created by Umaid Saleem on 7/6/21.
//

import XCTest
@testable import GMotorTest

class GetArtistsListServiceControllerTests: XCTestCase {

    private var sut: GetArtistsListServiceController?

    override func setUp() {
        super.setUp()
        sut = GetArtistsListServiceController()
    }

    override func tearDown() {
        super.tearDown()
    }
    
    func testSuccess() {
        NetworkManagerMock.testingParams = ["GetArtistListDataSuccess"]
        sut?.getArtistListData(artistName: "Alex", { (response) in
            switch response {
            case .success(_):
                break
            case .failure(_):
                XCTFail()
            }
        })
    }
    
    func testFailure() {
        NetworkManagerMock.testingParams = ["TestFailure"]
        sut?.getArtistListData(artistName: "Alex", { (response) in
            switch response {
            case .success(_):
                XCTFail()
            case .failure(_):
                break
            }
        })
    }
    
    func testNilData() {
        NetworkManagerMock.testingParams = ["TestNilData"]
        sut?.getArtistListData(artistName: "Alex", { (response) in
            switch response {
            case .success(_):
                XCTFail()
            case .failure(let error):
                XCTAssertEqual(error.code, nil)
                XCTAssertEqual(error.message, StringConstants.ErrorMessage.noData)
            }
        })
    }

    func testJSONDecodingError() {
        NetworkManagerMock.testingParams = ["JSONDecodingError"]
        sut?.getArtistListData(artistName: "Alex", { (response) in
            switch response {
            case .success(_):
                XCTFail()
            case .failure(let error):
                XCTAssertEqual(error.code, nil)
                XCTAssertEqual(error.message, StringConstants.ErrorMessage.jsonDecodingError)
            }
        })
    }
}

